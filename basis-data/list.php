<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "makul_basis_data_1";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM user";
$result = $conn->query($sql);



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Penggun</title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>No</td>
            <td>Nama Depan</td>
            <td>Nama Belakang</td>
        </tr>
        <?php
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
        ?>
                <tr>
                    <td><?= $row["user_id"] ?></td>
                    <td><?= $row["user_first_name"] ?></td>
                    <td><?= $row["user_last_name"] ?></td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr>
                <td colspan=3>Belum ada data</td>
            </tr>
        <?php
        }
        $conn->close();
        ?>

    </table>
</body>

</html>