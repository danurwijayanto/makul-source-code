import express from 'express';
import { createServer } from 'node:http';
import { fileURLToPath } from 'node:url';
import { dirname, join } from 'node:path';
import { Server } from 'socket.io';

const app = express();
const server = createServer(app);
const io = new Server(server);

const __dirname = dirname(fileURLToPath(import.meta.url));

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'index.html'));
});

app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});

var publicNsp = io.of('/public');
var privateNsp = io.of('/private');

publicNsp.on('connection', (socket) => {
  console.log('a user connected to public Nsp');
  publicNsp.emit('chat message', 'hellow public');

  socket.on('chat message', (msg) => {
    publicNsp.emit('chat message', msg);
    console.log('message publicNsp: ' + msg);
  });
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});

privateNsp.on('connection', (socket) => {
  console.log('a user connected to privatye Nsp');
  privateNsp.emit('chat message', 'hellow private');

  socket.on('chat message', (msg) => {
    privateNsp.emit('chat message', msg);
    console.log('message privateNsp: ' + msg);
  });
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});

server.listen(3000, () => {
  console.log('server running at http://localhost:3000');
});